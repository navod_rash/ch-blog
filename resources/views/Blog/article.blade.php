@extends('Admin.layout')

@section("styles")
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/ui/dragula.min.css") }}">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>

@endsection

@section('content')
    @include('Admin.partials.breadcumbs',['header' => 'Blog'])

    <div class="content-body">

        <div class="row">
            <div class="col-md-12 form-card">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"
                            id="basic-layout-form-center">{{ (request()->get('translate',false)) ? 'Translate Post - '. Creativehandles\ChBlog\Plugins\Blog\Models\BlogArticle::find(request()->get('parent_article',0))->post_title : __('blog.Create a new blog post')}}</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form action="{{route('admin.createNewArticle')}}" method="post" id="postForm"
                                  enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-body">

                                            {{csrf_field()}}
                                            <input type="hidden" name="translate"
                                                   value="{{request()->get('translate',false)}}">
                                            <input type="hidden" name="parent_article"
                                                   value="{{request()->get('parent_article','')}}">
                                            {{--title--}}
                                            <div class="form-group">
                                                <label>{{__("blog.Post Title")}}</label>
                                                <input required type="text" class="form-control" id="post_title"
                                                       placeholder="Post Title"
                                                       name="title" value="{{old('title')}}">
                                            </div>

                                            <div class="form-group ">
                                                <label for="authorList">{{__('blog.Author')}}</label>
                                                <select required class="select2 form-control" id="authorList"
                                                        name="author">
                                                    <option value="" disabled selected> {{__('blog.Select Author')}} </option>
                                                    @foreach($authors as $author)
                                                        <option value="{{$author->id}}">
                                                            <span>{{ $author->first_name .' ' . $author->last_name .' - '}}</span> {{ $author->user_rank }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <input type="hidden" name="source_id" id="source_id">
                                            {{--url--}}
                                            <div class="form-group">
                                                <label>{{__('blog.Post URL')}}
                                                    <a title="show preview" id="show-preview" class="hide show-url-preview"
                                                       href="{{ \Creativehandles\ChBlog\ChBlogFacade::buildPreviewUrl('', $availableId ?? 1, '') }}"
                                                       target="_blank">
                                                        <i class="fa fa-external-link-square"></i>
                                                    </a>
                                                </label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="span-url">{{ \Creativehandles\ChBlog\ChBlogFacade::buildPreviewUrl('', $availableId ?? 1, '') }}/</span>
                                                    </div>
                                                    <input required type="text" class="form-control" id="post_slug"
                                                           placeholder="Post URL"
                                                           name="post_slug" value="{{old('post_slug')}}">
                                                </div>

                                            </div>

                                            {{--meta-title--}}
                                            <div class="form-group">
                                                <label>{{__('blog.Meta Title')}}</label>
                                                <input type="text" class="form-control"
                                                       placeholder="Meta Title"
                                                       name="meta_title" value="{{old('meta_title')}}">
                                            </div>

                                            {{--meta description--}}
                                            <div class="form-group">
                                                <label>{{__('blog.Meta description')}}</label>
                                                <textarea type="text" class="form-control"
                                                          placeholder="Meta description"
                                                          name="meta_description"
                                                          rows="3">{{old('meta_description')}}</textarea>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>{{__('blog.Post date')}}</label>
                                                    <input type="datetime" name="post_date" class="form-control"
                                                           value="{{old('post_date')}}" placeholder="Article Date">
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{__('blog.Read time')}}</label>
                                                        <input type="text" name="post_read_time" class="form-control"
                                                               value="{{old('read_time')}}"
                                                               placeholder="Estimated article read time">
                                                    </div>
                                                </div>

                                            </div>


                                            {{--featured type--}}
                                            <div class="form-group">
                                                <label>{{__('blog.Article Featured Media Type')}}</label>
                                                <fieldset class="radio">
                                                    <label>
                                                        <input type="radio" checked required name="feature_type"
                                                               value="img" class="feature_type">
                                                        {{__('blog.Image')}}
                                                    </label>
                                                </fieldset>
                                                <fieldset class="radio">
                                                    <label>
                                                        <input type="radio" required name="feature_type" value="vdo"
                                                               class="feature_type">
                                                        {{__('blog.Video')}}
                                                    </label>
                                                </fieldset>
                                            </div>

                                            {{--featured image--}}
                                            <div class="form-group hide" id="feature-img-container">
                                                <label>{{__('blog.Article Featured Image')}}</label>
                                                <input id="article_image" type="file" class="form-control"
                                                       name="article_image">
                                                <img id="article_image_preview" class="hide height-150 img-thumbnail">
                                            </div>

                                            {{--featured image--}}
                                            <div class="form-group hide" id="feature-vdo-container">
                                                <label>{{__('blog.Article Featured Video')}}</label>
                                                <input id="" type="text" class="form-control"
                                                       placeholder="Place Youtube Video URL" name="article_video">
                                            </div>

                                            <div class="form-group ">
                                                <label for="categorySelector">{{__('blog.Post category')}}</label>
                                                <select class="select2 form-control" id="categoryList" multiple
                                                        name="categories[]">
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}">
                                                            <span>{{ ($category->parent)? $category->parent->category_name.' - ':''}}</span> {{$category->category_name}}
                                                        </option>
                                                    @endforeach
                                                </select>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="#" id="newCategory"><b>{{__('blog.New category?')}}</b></a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    {{--language--}}
                                                    <div class="form-group">
                                                        <label for="language">{{__('blog.Language')}}</label>
                                                        <select required class="custom-select block" name="language"
                                                                id="language">
                                                            <option disabled>{{__('blog.Please select a value')}}</option>

                                                            @if(request()->get('translate',false))
                                                                @foreach(config('laravellocalization.supportedLocales') as $langs)
                                                                    <option {{(Creativehandles\ChBlog\Plugins\Blog\Models\BlogArticle::find(request()->get('parent_article'))->post_language == $langs['short_name']) ? 'disabled' : ''}} value="{{$langs['short_name']}}">
                                                                        {{ $langs['name'] }}
                                                                    </option>
                                                                @endforeach
                                                            @else
                                                                @foreach(config('laravellocalization.supportedLocales') as $langs)
                                                                    <option {{(app()->getLocale() == $langs['short_name']) ? 'selected' : ''}} value="{{$langs['short_name']}}">
                                                                        {{ $langs['name'] }}
                                                                    </option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {{--article visibility--}}
                                                    <div class="form-group">
                                                        <label>{{__('blog.Post visibility')}}</label>
                                                        <select required class="custom-select block" name="visibility">
                                                            <option disabled>{{__('blog.Please select a value')}}</option>
                                                            <option @if (old('visibility')) {{ (old('visibility') == '1') ? 'selected' : '' }} @else selected
                                                                    @endif value="1">{{__('blog.Visible to all')}}
                                                            </option>
                                                            <option @if (old('visibility')) {{ (old('visibility') == '2') ? 'selected' : '' }}  @endif value="2">
                                                                {{__('blog.Hidden for public')}}
                                                            </option>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--body--}}
                                            <div class="form-group">
                                                <label>{{__('blog.Post body')}}</label>
                                                <textarea name="body" id="postBody" cols="30" rows="10"
                                                          placeholder="body"
                                                          class="form-control summernote-body"></textarea>
                                            </div>
                                            {{--link to categories--}}
                                            <div class="form-group">
                                                <label>{{__('blog.Post Tags')}}</label>
                                                <select class=" form-control" id="tagSelector"
                                                        multiple="multiple" aria-hidden="true"
                                                        name="tags[]">
                                                    @foreach($tags as $tag)
                                                        <option @if(is_array(old('tags')) && in_array($tag->label, old('tags'))) selected
                                                                @endif value="{{$tag->label}}">{{$tag->label}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-actions left">
                                            <button type="reset" class="btn btn-warning mr-1">
                                                <i class="ft-x"></i> {{__('blog.Clear')}}
                                            </button>
                                            <a href="{{ URL::previous() }}"><button type="button" href="" class="btn btn-warning mr-1">
                                                <i class="ft-arrow-left"></i> {{__('blog.Go Back')}}
                                            </button></a>
                                            <button type="button" class="btn btn-primary" id="saveForm">
                                                <i class="fa fa-check-square-o"></i> {{__('blog.Save')}}
                                            </button>
                                            {{--<button type="submit" class="btn btn-primary" id="saveAndGoBack">
                                                <i class="fa fa-check-square-o"></i> {{__('blog.Save and Go back')}}
                                            </button>--}}
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card hide" id="category-card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form-center">{{__('blog.Create new Category')}}</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    @include('Admin.Blog.new-category-form')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/extensions/dragula.min.js") }}" type="text/javascript"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            var changeSelector = '.show-url-preview';
            //app url
            var feBaseUrl = "{{ \Creativehandles\ChBlog\ChBlogFacade::buildFeBaseUrl() }}";
            var urlPreview = $(changeSelector).attr('href');

            //set url on page load
            var currLocale = $('#language').val();
            var replacedUrl = urlPreview.replace(feBaseUrl, feBaseUrl + '/' + currLocale);
            $(changeSelector).attr('href', replacedUrl);

            var spanCurUrl = $('#span-url').html();
            $('#span-url').html(spanCurUrl.replace(feBaseUrl, feBaseUrl + '/' + currLocale));

            //set url when change page locale
            $('#language').on('change',function(){
                var currLocale = $(this).val();
                urlPreview = $(changeSelector).attr('href');
                var res = urlPreview.replace(feBaseUrl, feBaseUrl + '/' + currLocale);
                //set href
                $(changeSelector).attr('href', spanCurUrl.replace(feBaseUrl, feBaseUrl + '/' + currLocale) + $('#post_slug').val());

                $('#span-url').html(spanCurUrl.replace(feBaseUrl, feBaseUrl + '/' + currLocale));
            });

            //change url on title change
            $('#post_title').on('keyup', function () {
                var articletitle = $('#post_title').val();
                $('#post_slug').val($.slugify(articletitle));
            });

            $('#post_slug').on('keyup', function () {
                $('#post_slug').val($.slugify($(this).val()));
            });

            //read url for image
            function readURL(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        // console.log(e.target.result);
                        // $("#article_image").append('<img class="height-150 img-thumbnail" src="'+e.target.result+'">')
                        $('#article_image_preview').attr('src', e.target.result);
                        $('#article_image_preview').show();
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#article_image").change(function () {
                readURL(this);
            });

            var checkedFeature = $('.feature_type').val();

            if (checkedFeature == 'img') {
                $('#feature-vdo-container').hide();
                $('#feature-img-container').show();
            } else {
                $('#feature-img-container').hide();
                $('#feature-vdo-container').show();
            }

            $('.feature_type').on('change', function () {
                var checkedValue = $(this).val();

                if (checkedValue == 'img') {
                    $('#feature-vdo-container').hide();
                    $('#feature-img-container').show();
                } else {
                    $('#feature-img-container').hide();
                    $('#feature-vdo-container').show();
                }
            });

            $('#category-name').on('keyup', function () {
                var cat_name = $('#category-name').val();
                $('#seo_title').val(cat_name);
                $('#category_slug').val($.slugify(cat_name));
            });

            $('#category-description').on('keyup', function () {
                $('#seo_description').val($('#category-description').val());
            });

            $('#parentCategory').select2({
                placeholder: 'Select a parent category',
                closeOnSelect: true,
                allowClear: true,
            });

            $('#categoryList').select2({
                placeholder: 'Select a category',
                closeOnSelect: true,
                allowClear: true,
            });

            $('#newCategory').on('click', function (e) {
                e.preventDefault();
                $('.form-card').removeClass('col-md-12');
                $('.form-card').addClass('col-md-8');
                $('#category-card').toggle();
            });

            //checkbox require  validation
            function checkboxRequired() {
                var requiredCheckboxes = $('.options :checkbox[required]');

                if (requiredCheckboxes.is(':checked')) {
                    requiredCheckboxes.removeAttr('required');
                } else {
                    requiredCheckboxes.attr('required', 'required');
                }

                requiredCheckboxes.change(function () {
                    if (requiredCheckboxes.is(':checked')) {
                        requiredCheckboxes.removeAttr('required');
                    } else {
                        requiredCheckboxes.attr('required', 'required');
                    }
                });
            };

            checkboxRequired();

            //new category form submission and append added category to article form
            var categoryForm = $('#category-form');

            categoryForm.on('submit', function (e) {
                e.preventDefault();

                $.ajax({
                    data: categoryForm.serialize(),
                    dataType: 'json',
                    method: 'post',
                    url: "{{ route("admin.createNewCategory") }}",
                }).done(function (response) {
                    toastr.success('New category added', 'Success');
                    var data = response.data;

                    $('#categoryList').last().append(
                        `<option selected value="` + data.id + `">` + ((data.parent) ? data.parent + ' - ' : '') + data.category_name + `</option>`
                    );

                    $('.options :checkbox[required]').removeAttr('required');
                    categoryForm[0].reset();
                }).fail(function (error) {
                    var messages = error.responseJSON.msg;
                    for (message in messages) {
                        toastr.error(messages[message], 'Error',{ timeOut: 5000  });
                    }
                });
            });


            //select 2 tag initiator
            $('#tagSelector').select2({
                placeholder: 'Select tags',
                tags: true
            });

            //select2 initiator
            $('#categorySelector').select2({
                placeholder: 'Select categories',
                minimumResultsForSearch: Infinity,
            });
            //select2 initiator
            $('#authorList').select2({
                placeholder: 'Select an author',
                closeOnSelect: true,
                allowClear: true,
            });

            //new article form submission
            var postUrl = $('#show-preview').attr('href') + "/";

            var saveFormBtn = $('#saveForm');
            var saveBtnOriginal = saveFormBtn.html();

            $('#saveForm').on('click', function (e) {
                e.preventDefault();
                saveFormBtn.html('<i class="fa fa-spinner"></i> Processing');
                //new article form submission
                var postForm = $('#postForm')[0];

                // Create an FormData object
                var data = new FormData(postForm);
                $.ajax({
                    data: data,
                    dataType: 'json',
                    method: 'post',
                    processData: false,  // Important!
                    contentType: false,
                    url: "{{ route("admin.createNewArticle") }}",
                }).done(function (response) {
                    $('#source_id').val(response.data.id);

                    $('#show-preview').attr('href', $('#span-url').html() + $('#post_slug').val());
                    $('#show-preview').show();
                    saveFormBtn.html(saveBtnOriginal);
                    toastr.success('New article Saved', 'Success', {
                        timeOut: 5000,
                        fadeOut: 1000,
                        progressBar: true,
                    });

                }).fail(function (error) {
                    saveFormBtn.html(saveBtnOriginal);
                    var messages = error.responseJSON.msg;

                    for (message in messages) {
                        toastr.error(messages[message], window.errorMsg);
                    }

                });
            });

            //form reset button functionality
            $("button[type='reset']").on("click", function (event) {
                event.preventDefault();
                postForm[0].reset();
                $('#postBody').summernote("reset");
                $('#tagSelector').val('').trigger('change');
            });
        });
    </script>
@endsection