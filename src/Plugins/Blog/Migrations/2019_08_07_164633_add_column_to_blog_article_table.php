<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToBlogArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog_article', function (Blueprint $table) {
            $table->integer('parent_lang_post')->nullable();
            $table->tinyInteger('translated_article')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_article', function (Blueprint $table) {
           $table->dropColumn('parent_lang_post');
           $table->dropColumn('translated_article');
        });
    }
}
