<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 2/5/19
 * Time: 7:28 PM
 */

namespace Creativehandles\ChBlog\Plugins\Blog\Repositories;


use Creativehandles\ChBlog\Plugins\Blog\Blog;
use Creativehandles\ChBlog\Plugins\Blog\Models\BlogArticle;
use App\Repositories\BaseEloquentRepository;

class BlogArticleRepository extends BaseEloquentRepository
{

    public function __construct(BlogArticle $model)
    {
        $this->model = $model;
    }

    /**
     * Following functions are used to get data for frontend. so that HIDDEN_TO_PUBLIC is used to restrict hidden posts
     */

    /**
     * @param $orderByColumn
     * @param $orderByType
     * @param array $with
     * @param int $limit
     * @param $field
     * @param $condition
     * @param $value
     * @param int $visibility
     * @return mixed
     */
    public function getRecentArticlesExceptReadingOne(
        $orderByColumn,
        $orderByType,
        $with = [],
        $limit = 100,
        $field,
        $condition,
        $value,
        $visibility = Blog::HIDDEN_TO_PUBLIC
    ) {

        return $this->model
            ->where($field, $condition, $value)
            ->where('post_visibility', '!=', $visibility)
            ->where('post_language','=',app()->getLocale())
            ->with($with)
            ->orderBy($orderByColumn, $orderByType)
            ->limit($limit)
            ->get();

    }


    /**
     * @param null $orderBy
     * @param null $limit
     * @param array $with
     * @param int $visibility
     * @return mixed
     */
    public function getRecentArticles($orderBy = null, $limit = null, $with = [], $visibility = Blog::HIDDEN_TO_PUBLIC)
    {

        $model = $this->model
            ->where('post_visibility', '!=', $visibility)
            ->where('post_language','=',app()->getLocale())
            ->with($with);
        if ($orderBy) {
            $model->orderBy($orderBy, 'DESC');
        }

        if ($limit) {
            $model = $model->limit($limit);
        }
        return $model->get();
    }


    public function getTranslatedArticlesFor($articleId,$with = [],$visibility = Blog::HIDDEN_TO_PUBLIC)
    {
        //get article
        $model = $this->model
            ->where('parent_lang_post',$articleId)
            ->where('post_visibility', '!=', $visibility)
            ->with($with)
            ->get();

        return $model;

    }

    public function allWithoutPagination()
    {
        return $this->model->orderBy("updated_at", "DESC")->get();
    }

    public function getArticleByIdAndLocale($id, $locale)
    {
        return $this->model->where(function ($query) use ($id, $locale) {
            $query->where('id', $id)
                ->where('post_language', $locale);
        })->orWhere(function ($query) use ($id, $locale) {
            $query->where('parent_lang_post', $id)
                ->where('post_language', $locale);
        })->first();
    }
}