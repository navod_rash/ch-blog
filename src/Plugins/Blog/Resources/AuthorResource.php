<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 15/5/19
 * Time: 4:06 PM
 */

namespace Creativehandles\ChBlog\Plugins\Blog\Resources;


use App\Http\Resources\PostResource;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthorResource extends JsonResource
{

    public function toArray($request)
    {
        $resource = [
            'id'=>$this->id,
            'name'=>$this->name,
            'posts'=>PostResource::collection($this->whenLoaded('articles'))
        ];

        return $resource;
    }
}