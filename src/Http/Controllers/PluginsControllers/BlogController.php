<?php

namespace Creativehandles\ChBlog\Http\Controllers\PluginsControllers;

use App\Traits\MultilanguageTrait;
use Creativehandles\ChBlog\Plugins\Blog\Models\BlogArticle;
use Creativehandles\ChBlog\Plugins\Blog\Resources\ArticleResource;
use App\Repositories\CoreRepositories\Contracts\UserRepositoryInterface;
use App\Traits\UploadTrait;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Creativehandles\ChBlog\Plugins\Blog\Blog as Blog;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use ReflectionClass;

class BlogController extends Controller
{
    use MultilanguageTrait, UploadTrait;

    protected $blog;
    protected $views = 'Admin.Blog.';
    protected $logprefix = "BlogController";
    protected $eventmodel;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(Blog $blog, UserRepositoryInterface $userRepository)
    {
        $this->setLocaleInRequestHeader();

        $this->blog = $blog;
        $reflector = new ReflectionClass(BlogArticle::class);
        $this->eventmodel = $reflector->getShortName();
        $this->userRepository = $userRepository;
    }


    /**
     * render blog plugin
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->articles();
    }

    /**
     * new article view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createArticle()
    {
        $tags = $this->blog->getAllTags($this->eventmodel);
        $categories = $this->blog->getAllCategories();
        $availableId = $this->blog->getNextkey();
        $authors = $this->userRepository->getAuthors();
        return view($this->views."article")->with(compact('tags', 'categories', 'availableId', 'authors'));
    }

    /**
     * store new article
     *
     */
    public function storeArticle(Request $request)
    {
        //validate input data
        $validator = $this->validateArticleData($request);

        if ($validator->fails()) {
            Log::error($this->logprefix.'('.__FUNCTION__.'): Article data incomplete ', $request->all());

            if ($request->ajax()) {
                return response()->json(['msg' => $validator->errors()->toArray()], 400);
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }


        //format input data
        list($formattedArticle, $categories, $tags) = $this->formatArticleData($request);


        if ($request->has('article_image')) {
            // Get image file
            $image = $request->file('article_image');

            // Make a image name based on title and current timestamp
            $name = str_slug($request->input('title')).'_'.time();
            // Define folder path
            $folder = '/uploads/images/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder.$name.'.'.$image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set image path in database to filePath
            $formattedArticle['post_image'] = $filePath;
        }

        try {
            if ($request->get('source_id')) {
                $formattedArticle['id'] = $request->get('source_id');
                $storedArticle = $this->blog->updateArticle($formattedArticle);
            }

            $storedArticle = $this->blog->storeNewArticle($formattedArticle);

            //link categories
            $storedArticle->categories()->sync($categories);
            //link tags
            if ($tags) {
                $taglist = [];
                foreach ($tags as $key => $value) {
                    $taglist[$value] = [
                        'model' => $this->eventmodel,
                        'ordinal' => $key + 1
                    ];
                }

                $storedArticle->tags()->where('model', $this->eventmodel)->sync($taglist);
            } else {
                $storedArticle->tags()->where('model', $this->eventmodel)->detach();
            }

        } catch (Exception $e) {

            if ($request->ajax()) {
                return response()->json(['msg' => "Error Occured", 'developer_msg' => $e->getMessage()], 500);
            } else {
                return redirect()->back()->withInput()->with('error', 'Error Occured')->withInput();
            }

        }

        if ($request->ajax()) {
            return response()->json(['msg' => "Article Saved", 'data' => $storedArticle], 200);
        } else {
            return redirect()->route('admin.blog')->with('success', 'Article created successfully!');
        }

    }

    /**
     *list all articles
     */
    public function articles()
    {
        $articles = $this->blog->getAllArticles();
        return view($this->views."article-list")->with(['articles' => $articles]);
    }


    /**
     * view single article
     * @param $articleSlug
     * @return $this
     */
    public function showArticle($articleSlug)
    {
        $article = $this->blog->getArticleBySlug($articleSlug);

        return view($this->views.'article-show')->with(['article' => $article]);
    }

    public function searchArticle(Request $request)
    {
        $param = $request->input('term');

        $articles = $this->blog->getArticleByName(array_get($param, 'term', ''));

        $articles = $articles->map(function ($article) {

            //$article->id = $article->id;
            $article->text = $article->post_title;

            return collect($article->toArray())
                ->only(['id', 'post_slug', 'text'])
                ->all();
        });
        return response()->json($articles);
    }

    /**
     *article update view
     * @param $articleId
     * @return $this
     */
    public function editArticle($id)
    {
        $tags = $this->blog->getAllTags($this->eventmodel);
        $categories = $this->blog->getAllCategories();
        $article = $this->blog->getArticleById($id);
        $authors = $this->userRepository->getAuthors();
        return view($this->views."article-edit")->with(compact('tags', 'categories', 'article', 'authors'));
    }

    public function updateArticle(Request $request, $articleId)
    {
        //validate input data
        $validator = $this->validateArticleData($request);

        if ($validator->fails()) {
            Log::error($this->logprefix.'('.__FUNCTION__.'): Article data incomplete ', $request->all());

            if ($request->ajax()) {
                return response()->json(['msg' => $validator->errors()->toArray()], 400);
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }


        //format form input data
        list($formattedArticle, $categories, $tags) = $this->formatArticleData($request);


        if ($request->file('article_image')) {
            // Get image file
            $image = $request->file('article_image');

            // Make a image name based on title and current timestamp
            $name = str_slug($request->input('title')).'_'.time();
            // Define folder path
            $folder = '/uploads/images/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder.$name.'.'.$image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set image path in database to filePath
            $formattedArticle['post_image'] = $filePath;
        } else {
            $formattedArticle['post_image'] = $request->get('article_image_old');
        }

        //unset slug changes for the moment
//        unset($formattedArticle['post_slug']);


        try {
            $formattedArticle['id'] = $articleId;
            $updateArticle = $this->blog->updateArticle($formattedArticle);

            //link categories
            $updateArticle->categories()->sync($categories);

            //link tags
            if ($tags) {
                $taglist = [];
                foreach ($tags as $key => $value) {
                    $taglist[$value] = [
                        'model' => $this->eventmodel,
                        'ordinal' => $key + 1
                    ];
                }

                $updateArticle->tags()->where('model', $this->eventmodel)->sync($taglist);
            } else {
                $updateArticle->tags()->where('model', $this->eventmodel)->detach();
            }

        } catch (Exception $e) {
            if ($request->ajax()) {
                return response()->json(['msg' => "Error Occured", 'developer_msg' => $e->getMessage()], 500);
            } else {
                return redirect()->back()->withInput()->with('error', "Error Occured");
            }

        }
        if ($request->ajax()) {
            return response()->json(['msg' => "Article Saved"], 200);
        } else {
            return redirect()->route('admin.blog')->with('success', "Article Updated");
        }
    }

    public function deleteArticle($articleId)
    {
        $deletedArticle = $this->blog->deleteArticleById($articleId);

        if (!$deletedArticle) {
            return response()->json(['msg' => "Error Occured"], 400);
        }

        return response()->json(['msg' => "Article Deleted"], 200);
    }




    /**
     * Category related functions start
     */


    /**
     * new category view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createCategory()
    {
        $parentCat = request()->get('parentCat', null);
        $getChild = request()->get('getChild', null);

        if ($parentCat) {
            $categories = $this->blog->getCategoryById($parentCat)->children;
        } else {
            $categories = $this->blog->getAllCategories();
        }

        return view($this->views."category")->with(compact('categories', 'parentCat'));
    }

    /**
     * update/create new category
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeCategory(Request $request)
    {

        $validator = $this->validateCategory($request);

        if ($validator->fails()) {
            Log::error($this->logprefix.'('.__FUNCTION__.'): Article data incomplete ', $request->all());

            if ($request->ajax()) {
                return response()->json(['msg' => $validator->errors()->toArray()], 400);
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }

        $formattedCategory = $this->getFormattedCategory($request);

        $storedCategory = $this->blog->storeNewCategory($formattedCategory);

        if (!$storedCategory) {
            if ($request->ajax()) {
                return response()->json(['msg' => "Error Occured"], 400);
            } else {
                return redirect()->back()->withInput()->with('error', 'Error Occured');
            }
        }

        $category = $storedCategory->toArray();
        $category['parent'] = ($storedCategory->parent) ? $storedCategory->parent->category_name : null;

        if ($request->ajax()) {
            return response()->json(['msg' => "Category Saved", 'data' => $category], 200);
        } else {
            return redirect()->route('admin.createCategory')->with('success', "Category Saved");
        }
    }

    public function editCategory(Request $request, $categoryId)
    {
        $category = $this->blog->getCategoryById($categoryId);

        if ($request->ajax()) {
            if (isset($category)) {
                $category->setDefaultLocale($request->get('form_locale', app()->getLocale()));
                return response()->json(['data' => $category], 200);
            } else {
                return response()->json(['msg' => [__('category.404')]], 404);
            }
        }

        $categories = $this->blog->getAllCategories();
        return view($this->views.'category-edit')->with(compact('category', 'categories'));
    }

    public function updateCategory(Request $request, $categoryId)
    {
        $validator = $this->validateCategory($request);

        if ($validator->fails()) {
            Log::error($this->logprefix.'('.__FUNCTION__.'): Article data incomplete ', $request->all());

            if ($request->ajax()) {
                return response()->json(['msg' => $validator->errors()->toArray()], 400);
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }

        $formattedCategory = $this->getFormattedCategory($request);
        $formattedCategory['id'] = $categoryId;

        // unset slug
        // un-setting slug is removed as different languages should have language specific slugs
        // unset($formattedCategory['category_slug']);

        $updatedCategory = $this->blog->updateCategory($formattedCategory);

        if (!$updatedCategory) {
            if ($request->ajax()) {
                return response()->json(['msg' => "Error Occured"], 400);
            } else {
                return redirect()->back()->withInput()->with('error', "Error Occured");
            }
        }
        if ($request->ajax()) {
            return response()->json(['msg' => "Category Updated"], 200);
        } else {
            return redirect()->route('admin.createCategory')->with('success', "Category Updated");
        }
    }

    public function deleteCategory($categoryId)
    {
        $deletedCategory = $this->blog->deleteCategory($categoryId);

        if (!$deletedCategory) {
            return response()->json(['msg' => "Error Occured"], 400);
        }

        return response()->json(['msg' => "Category Deleted"], 200);
    }

    /**
     * get all categories by ajax call
     * @return mixed
     */
    public function getAjaxCategories()
    {
        $categories = $this->blog->getAllCategories();

        return $categories->pluck('category_name', 'id')->all();
    }

    /**
     * article input data validation
     *
     */
    protected function validateArticleData(Request $request)
    {
        $rules = [
            'title' => 'required',
            'language' => 'required',
            'body' => 'required',
            'author' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        return $validator;

    }

    /**
     * @param  Request  $request
     * @return array
     */
    public function formatArticleData(Request $request): array
    {
        $body = preg_replace('/src="..\/..\//', 'src="../../../', $request->body);

        $formattedArticle = [
            'post_title' => $request->title,
            'post_slug' => ($request->post_slug) ? str_slug($request->post_slug) : str_slug($request->title),
            'post_meta_title' => ($request->meta_title) ? $request->meta_title : $request->title,
            'post_meta_description' => ($request->meta_description) ? strip_tags($request->meta_description) : strip_tags(mb_substr(iconv('utf-8',
                'utf-8', html_entity_decode(str_replace('<br>', ' ', $request->body), ENT_QUOTES, 'UTF-8')), 0, 300,
                'utf-8')),
            'post_language' => $request->language,
            'post_visibility' => $request->visibility,
            'post_body' => $body,
            'post_author' => $request->author,
            'post_feature_media_type' => $request->feature_type,
            'post_video' => $request->article_video,
            'post_read_time' => $request->post_read_time,
            'post_date' => ($request->post_date) ? Carbon::parse($request->post_date)->toDateTimeString() : null,
            'id' => $request->source_id ?? null
        ];

        if ($request->input('translate', false)) {
            $formattedArticle['parent_lang_post'] = $request->input('parent_article', false);
            $formattedArticle['translated_article'] = true;
        }

        $categories = $request->categories;

        $tags = $request->input('tags', []);

        foreach ($tags as $key => $tag) {
            if (is_string($tag)) {

                $data = [
                    'model' => $this->eventmodel,
                    'locale' => $request->form_locale,
                    'label' => $tag,
                    'author' => \Auth::id(),
                    'color' => sprintf('#%06X', mt_rand(0, 0xFFFFFF))
                ];

                $condition = [
                    'model' => $this->eventmodel,
                    'locale' => $request->form_locale,
                    'label' => $tag
                ];

                $storedTag = $this->blog->createNewTag($data, $condition);
                $tags[$key] = $storedTag->id;
            }
        }

        return array($formattedArticle, $categories, $tags);
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse|null
     */
    protected function validateCategory(Request $request)
    {
        $rules = [
            'category-name' => 'required',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * @param  Request  $request
     * @return array
     */
    protected function getFormattedCategory(Request $request): array
    {
        return [
            'locale' => $request->input('form_locale'),
            'category_name' => $request->input('category-name'),
            'parent_id' => ($request->input('parent_category') != null) ? $request->input('parent_category') : 0,
            'category_description' => $request->input('category-description'),

            'seo_title' => $request->input('seo_title'),
            'seo_description' => $request->input('seo_description'),
        ];
    }

    /**
     * Blog Frontend related functions
     */
    public function fetchAllArticles()
    {
        //fetch all articles
        $articles = $this->blog->getAllArticles();
        //go through resource
        return ArticleResource::collection($articles);
    }

    public function storeImage(Request $request)
    {
        $file = $request->file('file');
        $newfilename = rand(0, 9999).'_'.time().'_'.$file->getClientOriginalExtension();
        $path = url('/uploads/images').'/'.$newfilename;
        $imgpath = $file->move(public_path('/uploads/images/'), $newfilename);
        $fileNameToStore = $path;


        return response()->json(['location' => $fileNameToStore]);

    }
}
