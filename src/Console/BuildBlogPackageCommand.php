<?php

namespace Creativehandles\ChBlog\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class BuildBlogPackageCommand extends Command
{
    protected $signature = 'creativehandles:build-blog-plugin';

    protected $description = 'Build all prerequisites to start the plugin';

    public function handle()
    {
        $plugin = 'Blog';

        $this->info('Publising vendor directories');
        $this->callSilent('vendor:publish', ['--provider' => 'Creativehandles\ChBlog\ChBlogServiceProvider']);

        $this->info('Migrating tables');
        //run migrations in the plugin
        Artisan::call('migrate --path=app/Plugins/Blog/Migrations');

        $this->info('Migration complete');

        //seed active plugins table
        if (! DB::table('active_plugins')->where('plugin', $plugin)->first()) {
            DB::table('active_plugins')->insert([
                'plugin'=>$plugin,
            ]);
        }

        $this->info('Good to go!!');
    }
}
