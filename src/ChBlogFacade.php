<?php

namespace Creativehandles\ChBlog;

use Illuminate\Support\Facades\Facade;

/**
 * @method static ChPages buildFeBaseUrl()
 * @method static ChPages buildPreviewUrl(string $locale, int $pageId, string $pageSlug)
 *
 * @see \Creativehandles\ChBlog\Skeleton\SkeletonClass
 */
class ChBlogFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ch-blog';
    }
}
