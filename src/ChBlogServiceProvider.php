<?php

namespace Creativehandles\ChBlog;


use Creativehandles\ChBlog\Console\BuildBlogPackageCommand;
use Illuminate\Support\Facades\Artisan;
use \Route;
use Illuminate\Support\ServiceProvider;

class ChBlogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'ch-blog');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'ch-blog');
         $this->loadMigrationsFrom(__DIR__.'/Plugins/Blog/Migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('ch-blog.php'),
            ], 'config');


            // Publishing the blog controller.
            $this->publishes([
                __DIR__.'/../app/Http/Controllers/' => app_path('/Http/Controllers/'),
            ], 'pluginController');



            //publishing routes and breadcrumbs
            $this->publishes([
                __DIR__.'/../routes' => base_path('routes/packages'),
            ], 'routes');

            // Publishing the views.
            $this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/Admin/'),
            ], 'views');

            // Publishing assets.
            /*$this->publishes([
                __DIR__.'/../resources/assets' => public_path('vendor/ch-blog'),
            ], 'assets');*/

            // Publishing the translation files.
            /*$this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/ch-blog'),
            ], 'lang');*/

            // Registering package commands.
             $this->commands([
                 BuildBlogPackageCommand::class
             ]);

        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'ch-blog');


        // Register the main class to use with the facade
        $this->app->singleton('ch-blog', function () {
            return new ChBlog;
        });
    }
}
