<?php

/*
 * You can place your custom package configuration in here.
 */
return [
    'fe_base_url' => env('FE_BASE_URL', ''),
    'fe_blog_url' => env('FE_BLOG_URL', '')
];