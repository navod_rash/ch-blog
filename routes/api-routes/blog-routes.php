<?php

Route::group(['name' => 'blog', 'groupName' => 'blog'], function () {
    Route::get('/blog', 'APIControllers\PluginsControllers\BlogAPIController@getAllPosts')->name('getAllPosts');
    Route::get('/blog/{id}', 'APIControllers\PluginsControllers\BlogAPIController@getPost')->name('getPost');
});
