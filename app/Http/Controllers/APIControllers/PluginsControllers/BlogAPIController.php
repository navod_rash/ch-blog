<?php

namespace App\Http\Controllers\APIControllers\PluginsControllers;

/**
 * @group Blog
 *
 * APIs for blog
 */
class BlogAPIController extends \Creativehandles\ChBlog\Http\Controllers\APIControllers\BlogAPIController
{
}
